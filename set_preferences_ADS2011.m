% hpeesofdir points to the folder where ADS is installed
setpref('ADS','hpeesofdir',fullfile('C:','Agilent','ADS2011_01'));

% simarch indicates the computer architecture
setpref('ADS','simarch','win32_64');

% version indicates the installed ADS version
setpref('ADS','version','2011.01');

% the "simulationFolder" preference points to the folder where temporary simulation data can be stored.
setpref('ADS','simulationFolder',fullfile('D:','Simulation'));