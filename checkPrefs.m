function [SIMFOLDER,HPEESOFDIR,SIMARCH,ADSVERSION,PARALLEL,OUTPUT]=checkPrefs()
% this function checks whether the preferences are set.
%
%   [SIMFOLDER,HPEESOFDIR,SIMARCH,ADSVERSION,PARALLEL]=checkPrefs()
%
% The following preferences are needed in the ADStoolbox:
%
%   ADS.simulationFolder    folder which is used to store temporary files
%                           of the simulations. I use Z:\Simulation
%   ADS.hpeesofdir          Installation folder of ADS. on ELEC computers,
%                           this folder is C:\Agilent\ADS2011_01
%   ADS.simarch             simulation architecture of ADS. on ELEC
%                           computers, this is set to win32_64.
%   ADS.version             version of ADS running on the computer.
%   ADS.parallelSims        number of simulations that can be run in
%                           parallel on a computer.
%   ADS.output              this boolean controls whether the output of the
%                           simulations is shown on the command line.
%
% Adam Cooman, ELEC VUB

% check whether the ADS.simulationFolder preference is set. If this
% preference is not found, the function goes looking for an ADS install and
% sets the preferences itself if it finds one
if ispref('ADS','simulationFolder')
    SIMFOLDER = getpref('ADS','simulationFolder');
else
    % just try running the correct set_preferences_ADSxxxx myself
    try
        runSetPrefAutomatically();
        SIMFOLDER = getpref('ADS','simulationFolder');
    catch
        error(sprintf('Simulation folder has to be specified.\nRun the set_preferences_ADSxxxx script at least once on a new computer.'));
    end
    % check whether the SIMFOLDER exists, if not, make it
    if ~isdir(SIMFOLDER)
        mkdir(SIMFOLDER);
    end
end

if ispref('ADS','hpeesofdir')
    HPEESOFDIR = getpref('ADS','hpeesofdir');
else
    error('ADS.hpeesofdir preference not set.');
end

if ispref('ADS','simarch')
    SIMARCH = getpref('ADS','simarch');
else
    error('ADS.simarch preference not set.');
end

if ispref('ADS','version')
    ADSVERSION = getpref('ADS','version');
else
    ADSVERSION = '2013.06';
end

if ispref('ADS','parallelSims')
    PARALLEL = getpref('ADS','parallelSims');
else
    PARALLEL = 1;
end

if ispref('ADS','output')
    OUTPUT = getpref('ADS','output');
else
    OUTPUT = true;
end

end


function runSetPrefAutomatically()
% this function tries to run set_preferences_ADSxxx automatically on a
% computer that has ADS installed. If it fails, it just errors, so it
% should definitely be used in a try catch

% check the C:\Agilent directory
t = dir('C:\Agilent\');
% look for a folder called ADSxxx_xx
t=regexp({t.name},'ADS(?<year>\d+)_(?<version>\d+)','names');
t=t(cellfun(@(x) ~isempty(x),t));
% if that folder exists the version result is in t.version
if ~isempty(t)
    % go to the vagevuur folder
    currdir = cd();
    cd \\elec-stor1-w2k8\vagevuur\Toolbox\ADStoolbox\
    % run the correct set_preferences_ADSxxxx
    feval(['set_preferences_ADS' t{end}.year]);
    % go back to the original folder
    cd(currdir)
else
    error('to the catch!')
end
end
