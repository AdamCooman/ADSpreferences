% hpeesofdir points to the folder where ADS is installed
setpref('ADS','hpeesofdir',fullfile('C:','Agilent','ADS2014_01'));

% simarch indicates the computer architecture
setpref('ADS','simarch','win32_64');

% version indicates the installed ADS version
setpref('ADS','version','2014.01');

% the "simulationFolder" preference points to the folder where temporary simulation data can be stored.
setpref('ADS','simulationFolder',fullfile('C:','Simulation'));

% the "parallelSims" tells how many simulations can be run in parallel on a computer
setpref('ADS','parallelSims',1);

